#version 450 core

layout (triangles) in;
layout (triangle_strip, max_vertices=3) out;

layout(location = 0) in vec3 in_position[];
layout(location = 1) in vec3 in_normal[];
layout(location = 2) in vec2 in_texture_coords[];

layout(location = 0) out vec3 out_position;
layout(location = 1) out vec3 out_normal;
layout(location = 2) out vec2 out_texture_coords;

vec3 calculate_normal(const vec3 a, const vec3 b, const vec3 c) {
    return cross(b - a, c - a);
}

vec3 calculate_middle(const vec3 a, const vec3 b, const vec3 c) {
    return (a + b + c) / 3;
}

void main() {
    const vec3 position = calculate_middle(in_position[0], in_position[1], in_position[2]);
    const vec3 normal = calculate_normal(in_position[0], in_position[1], in_position[2]);
    
    for (int vertex_index = 0; vertex_index < 3; vertex_index += 1) {    
        gl_Position = gl_in[vertex_index].gl_Position;
        out_position = position;
        out_normal = normal;
        out_texture_coords = in_texture_coords[vertex_index];
        EmitVertex();
    }

    EndPrimitive();
}
