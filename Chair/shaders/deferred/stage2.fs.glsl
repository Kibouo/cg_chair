#version 450 core

const int MAX_AMOUNT_LIGHTS = 64;
// TODO: Adjust this to feel right. Depends on size of models.
const float LIGHT_RADIUS = 500.0;
const float AMBIENT_INTENSITY = 0.1;
struct Light {
    vec3 position;
    vec3 colour;
    float intensity;
};

layout (binding = 0) uniform sampler2D positions;
layout (binding = 1) uniform sampler2D normals;
// .a component is specular intensity
layout (binding = 2) uniform sampler2D diffuses_speculars;
layout (binding = 3) uniform sampler2D ambient_texture;
layout (binding = 4, std140) uniform LightBuffer { Light lights[MAX_AMOUNT_LIGHTS]; };

layout (location = 0) uniform int amount_lights;
layout (location = 1) uniform vec3 eye_position;

layout (location = 0) in vec2 in_texture_coords;

out vec4 out_colour;

// Blinn-Phong illumination
// Calculations are done in world space
void main() {
    vec4 diffuse_specular = texture(diffuses_speculars, in_texture_coords);

    const vec3 position = texture(positions, in_texture_coords).rgb;
    const vec3 raw_normal = texture(normals, in_texture_coords).rgb;
    const vec3 diffuse = diffuse_specular.rgb;
    const float shininess = diffuse_specular.a;
    const vec3 material_ambient = texture(ambient_texture, in_texture_coords).rgb;
    const vec3 material_specular = diffuse * 0.1;

    if (raw_normal == vec3(0.0))
        out_colour = vec4(0.02, 0.02, 0.02, 1.0);
    else {
        // Calculate lighting
        const vec3 normal = normalize(raw_normal);
        vec3 lighting = material_ambient * AMBIENT_INTENSITY;

        for (int i = 0; i < amount_lights; i++) {
            vec3 light_position = lights[i].position;
            float light_intensity = lights[i].intensity;
            vec3 light_direction_not_normalised = light_position - position;
            float light_distance = length(light_direction_not_normalised);

            // only calculate illumination in case the object is close enough
            if (light_distance < LIGHT_RADIUS) {
                // rename data
                vec3 light_colour = lights[i].colour;

                // pre-calc
                vec3 light_direction = normalize(light_direction_not_normalised);
                float cos_light_normal = dot(light_direction, normal);

                if (cos_light_normal > 0.0) {
                    // diffuse
                    vec3 diffuse_lighting = diffuse * light_colour * cos_light_normal;
                    // specular
                    vec3 half_vector = normalize(light_direction + normalize(eye_position - position));
                    vec3 specular_colour = material_specular * light_colour;
                    float specular_strength = pow(max(dot(normal, half_vector), 0.0), shininess);
                    vec3 specular_lighting = specular_colour * specular_strength;

                    // apply
                    float attenuation = pow(max(1.0 - light_distance/LIGHT_RADIUS, 0.0), 2);
                    lighting += light_intensity * attenuation * (diffuse_lighting + specular_lighting);
                }
            }
        }

        out_colour = vec4(lighting, 1.0);
    }
}
