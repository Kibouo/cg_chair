#ifndef CHAIR_VERTEXBUFFER_HPP
#define CHAIR_VERTEXBUFFER_HPP

#include "asset_tools/Mesh.hpp"
#include "glad.h"
#include <array>
#include <memory>
#include <vector>

class GpuMesh {
    GLuint vertex_array_object;
    GLuint vertex_buffer;
    GLuint index_buffer;

    GpuMesh(GpuMesh const &that) = delete;

  public:
    void draw() const;
    void bind_to_compute() const;
    GLuint index_count;

    void operator=(GpuMesh &&);

    GpuMesh(GpuMesh &&that);
    GpuMesh(asset_tools::Mesh const &mesh);
    ~GpuMesh();
};

#endif // CHAIR_VERTEXBUFFER_HPP
