#ifndef CHAIR_WINDOW_HPP
#define CHAIR_WINDOW_HPP

#include <GLFW/glfw3.h>
#include <memory>
#include <tuple>

class Window {
    Window(GLFWwindow *window);

  public:
    GLFWwindow *window;
    ~Window();
    static std::shared_ptr<Window> initialise();
    bool should_close() const;
    /**
     * Swaps the buffers, rendering a new frame. This also polls for events.
     * @return Whether the window was close-requested.
     */
    bool swap_buffers() const;
    float aspect_ratio() const;
    std::tuple<unsigned int, unsigned int> size() const;
};

#endif // CHAIR_WINDOW_HPP
