#ifndef CHAIR_TRANSFORMATION_HPP
#define CHAIR_TRANSFORMATION_HPP

#include "asset_tools/Component.hpp"
#include "entt/entity/registry.hpp"
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <iostream>
#include <map>
#include <utility>
#include <vector>

struct LocalTransformation {
    glm::vec3 translation;
    glm::vec3 rotation;
    glm::vec3 scaling;

    LocalTransformation(
            glm::vec3 const translation, glm::vec3 const rotation, glm::vec3 const scaling);
    LocalTransformation(asset_tools::component::Transformation const &transformation);
    LocalTransformation();

    glm::mat4 to_matrix() const;
};

struct Transformation {
    glm::mat4 matrix;
    Transformation();
    Transformation(glm::mat4 matrix);

    glm::vec3 translation() const;
};

class TransformationSystem {
    std::map<std::uint32_t, size_t> entity_indices;
    std::vector<std::uint32_t> entities;
    entt::DefaultRegistry &registry;

    void update_order();
    void update_locals();

  public:
    TransformationSystem(entt::DefaultRegistry &registry);
    void update_transformations();
};

#endif // CHAIR_TRANSFORMATION_HPP
