#ifndef CHAIR_SHADER_HPP
#define CHAIR_SHADER_HPP

#include "glad.h"
#include <filesystem>
#include <string>

/**
 * A wrapper around an OpenGL Shader that cleans up after itself.
 */
class Shader {
    GLuint shader_object;

    Shader() = delete;
    Shader(Shader const &) = delete;

    void print_compilation_log() const;
    bool compilation_was_success() const;

  protected:
    /**
     * Loads a new shader with the given source. Throws on compilation errors.
     */
    Shader(std::string const &shader_source, GLuint const shader_type);

    /**
     * Loads a new shader with the given file's contents as source. Throws on compilation errors.
     */
    Shader(std::filesystem::path const &shader_path, GLuint const shader_type);

  public:
    Shader(Shader &&);
    void operator=(Shader &&);
    virtual ~Shader();

    /**
     * Gets a raw object handle to the inner shader. This wrapper still owns it, do not delete it.
     */
    GLuint get_raw_object_handle() const;
};

struct VertexShader : public Shader {
    VertexShader(std::string const &shader_source);
    VertexShader(std::filesystem::path const &shader_path);
};

struct GeometryShader : public Shader {
    GeometryShader(std::string const &shader_source);
    GeometryShader(std::filesystem::path const &shader_path);
};

struct FragmentShader : public Shader {
    FragmentShader(std::string const &shader_source);
    FragmentShader(std::filesystem::path const &shader_path);
};

struct ComputeShader : public Shader {
    ComputeShader(std::string const &shader_source);
    ComputeShader(std::filesystem::path const &shader_path);
};

#endif
