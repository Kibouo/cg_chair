#ifndef CHAIR_FRAMERATECOMPENSATOR_HPP
#define CHAIR_FRAMERATECOMPENSATOR_HPP

#include <chrono>
#include <functional>

using namespace std::chrono;

double const FRAME_DURATION_MICROSEC = 100000.0 / 6.0;

// Instead of trying to simulate a portion of an action per frame,
// count the created time by the renderer and see how many simulations
// can be done in that time.
class FramerateCompensator {
    double accumulated_time = 0.0;
    time_point<system_clock> last_frame_time;

    FramerateCompensator(time_point<system_clock> const &last_frame_time)
        : last_frame_time(last_frame_time) {}

  public:
    static FramerateCompensator initialise();
    void count_created_time();
    void consume_time(std::function<void()> const &time_sensitive_fn);
};

#endif // CHAIR_FRAMERATECOMPENSATOR_HPP