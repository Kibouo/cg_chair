#ifndef CHAIR_RELATIONALCOMPONENTS_HPP
#define CHAIR_RELATIONALCOMPONENTS_HPP

template <typename Entity>
struct Parent {
    Entity identifier;
    Parent(Entity const parent) : identifier(parent) {}
};

// This component should be used to tag an entity as a base entity.
struct BaseEntity {};

#endif // CHAIR_RELATIONALCOMPONENTS_HPP