#ifndef CHAIR_RAYCASTER_HPP
#define CHAIR_RAYCASTER_HPP

#include "GpuMesh.hpp"
#include "ShaderProgram.hpp"
#include "Transformation.hpp"
#include "entt/entity/registry.hpp"
#include <tuple>

template <typename Entity>
struct RayCaster {
    static RayCaster<Entity>
    initialise(std::vector<GpuMesh> const &meshes, entt::Registry<Entity> &registry);
    glm::vec3 const calc_look_ray(
            Transformation const &camera_transformation,
            int const window_width_pixels,
            int const window_height_pixels,
            float const aspect_ratio) const;
    std::tuple<Entity, bool, float> const
    cast(Transformation const &camera_transformation, glm::vec3 const &look_ray);

  private:
    std::vector<GpuMesh> const &meshes;
    entt::Registry<Entity> &registry;
    ShaderProgram compute_shader;
    std::vector<std::tuple<std::tuple<Entity, bool, float>, GLuint, GLuint>> triangle_cast_data;

    RayCaster(
            std::vector<GpuMesh> const &meshes,
            entt::Registry<Entity> &registry,
            ShaderProgram compute_shader)
        : meshes(meshes), registry(registry), compute_shader(std::move(compute_shader)) {}
    Entity const base_entity_of(Entity const &entity) const;
    void initiate_triangle_level_cast(
            glm::vec3 const &origin,
            glm::vec3 const &look_ray,
            std::tuple<Entity, bool, float> const &entity_tuple);
    std::tuple<Entity, bool, float> evaluate_triangle_level_casts();
};

#endif // CHAIR_RAYCASTER_HPP