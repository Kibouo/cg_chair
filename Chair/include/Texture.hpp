#ifndef CHAIR_TEXTURE_HPP
#define CHAIR_TEXTURE_HPP

#include "asset_tools/Vector.hpp"
#include "glad.h"
#include <memory>

class Texture {
    GLuint texture_object;

    Texture(GLuint texture_object);
    Texture(Texture const &) = delete;

  public:
    static std::shared_ptr<Texture> from_file(char const *filename);
    static std::shared_ptr<Texture>
    from_data(GLvoid const *const data, GLuint width, GLuint height);
    static std::shared_ptr<Texture> from_colour(asset_tools::Vector<float, 3> const colour);
    Texture(Texture &&that);
    ~Texture();
    void operator=(Texture &&that);
    void bind(GLenum index) const;
};

#endif // CHAIR_TEXTURE_HPP
