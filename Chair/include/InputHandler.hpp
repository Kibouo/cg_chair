#ifndef CHAIR_INPUTHANDLER_HPP
#define CHAIR_INPUTHANDLER_HPP

#include "RayCaster.hpp"
#include "Renderer.hpp"
#include "Transformation.hpp"
#include "Window.hpp"
#include "entt/entity/registry.hpp"
#include <GLFW/glfw3.h>
#include <functional>
#include <glm/glm.hpp>
#include <unordered_map>
#include <vector>

float const SPEED = 5.0f;
float const SENSITIVITY = 0.0032f;

template <typename Entity>
using ToggleHandler =
        std::function<void(entt::Registry<Entity> &registry, Renderer &renderer, Entity player)>;

template <typename Entity>
class InputHandler {
    Window *const window_wrap;
    double last_xpos;
    double last_ypos;
    RayCaster<Entity> &ray_caster;

    bool mouse_1_pressable = true;
    bool mouse_2_pressable = true;
    std::unordered_map<int, bool> held_keys;
    std::unordered_map<int, std::vector<ToggleHandler<Entity>>> toggle_handlers;
    std::vector<int> keys_to_check;

    InputHandler(
            Window *const window_wrap,
            double const last_xpos,
            double const last_ypos,
            RayCaster<Entity> &ray_caster);

    void register_key_to_check(int key);
    void register_toggle_handler(int key, ToggleHandler<Entity> handler);

    void enact_walk_movement(glm::mat4 const &camera_model_matrix, glm::vec3 &translation) const;
    void forward(glm::mat4 const &camera_model_matrix, glm::vec3 &translation) const;
    void backward(glm::mat4 const &camera_model_matrix, glm::vec3 &translation) const;
    void left(glm::mat4 const &camera_model_matrix, glm::vec3 &translation) const;
    void right(glm::mat4 const &camera_model_matrix, glm::vec3 &translation) const;
    void up(glm::mat4 const &camera_model_matrix, glm::vec3 &translation) const;
    void down(glm::mat4 const &camera_model_matrix, glm::vec3 &translation) const;

    void switch_render_modes(Renderer &renderer) const;
    void enact_mouse_movement(glm::vec3 &axis_rotations);
    void tilt_y_axis(glm::vec3 &axis_rotations, float const angle) const;
    void tilt_x_axis(glm::vec3 &axis_rotations, float const angle) const;

    void enact_mouse_click(
            Transformation const &camera_transformation, entt::Registry<Entity> &registry);
    // uses ray tracing to interact with objects
    void
    enact_lmb(Transformation const &camera_transformation, entt::Registry<Entity> &registry) const;
    void
    enact_rmb(Transformation const &camera_transformation, entt::Registry<Entity> &registry) const;

    void enact_player_controls(Entity const &player, entt::Registry<Entity> &registry);
    void toggle_headlamp(Entity const &player, entt::Registry<Entity> &registry) const;

  public:
    static InputHandler<Entity>
    initialise(Window *const window_wrap, RayCaster<Entity> &ray_caster);

    void enact_all(entt::Registry<Entity> &registry, Renderer &renderer);
};

#endif // CHAIR_INPUTHANDLER_HPP
