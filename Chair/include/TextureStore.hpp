#ifndef CHAIR_TEXTURE_STORE_HPP
#define CHAIR_TEXTURE_STORE_HPP

#include "Texture.hpp"
#include <functional>
#include <map>
#include <memory>
#include <string>
#include <unordered_map>

class TextureStore {
    std::unordered_map<std::string, std::shared_ptr<Texture>> loaded_files;
    std::unordered_map<asset_tools::Vector<float, 3>, std::shared_ptr<Texture>> loaded_colours;

  public:
    TextureStore();
    std::shared_ptr<Texture> load_texture_from_file(std::string const &filename);
    std::shared_ptr<Texture> load_texture_from_colour(asset_tools::Vector<float, 3> colour);
};

#endif // CHAIR_TEXTURE_STORE_HPP
