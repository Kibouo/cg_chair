#ifndef CHAIR_BOUNDINGBOX_HPP
#define CHAIR_BOUNDINGBOX_HPP

#include "entt/entity/registry.hpp"
#include <glm/glm.hpp>
#include "asset_tools/Mesh.hpp"

struct BoundingBox {
    glm::vec3 min_corner;
    glm::vec3 max_corner;
    glm::vec3 const original_min_corner() const { return _original_min_corner; }
    glm::vec3 const original_max_corner() const { return _original_max_corner; }

    float const intersect(
            glm::vec3 const &translation,
            glm::vec3 const &ray_origin,
            glm::vec3 const &inv_look_ray) const;

    static BoundingBox from_mesh(asset_tools::Mesh const &&mesh);

    void operator=(BoundingBox const &&that);
    BoundingBox(BoundingBox const &that)
        : min_corner(that.min_corner), max_corner(that.max_corner),
          _original_min_corner(that.original_min_corner()),
          _original_max_corner(that.original_max_corner()) {}

  private:
    glm::vec3 _original_min_corner;
    glm::vec3 _original_max_corner;

    void set_original_min_corner(glm::vec3 const &new_value) {
        this->_original_min_corner = new_value;
    }
    void set_original_max_corner(glm::vec3 const &new_value) {
        this->_original_max_corner = new_value;
    }

    BoundingBox()
        : min_corner(glm::vec3(-1.0f, -1.0f, -1.0f)), max_corner(glm::vec3(1.0f, 1.0f, 1.0f)),
          _original_min_corner(glm::vec3(-1.0f, -1.0f, -1.0f)),
          _original_max_corner(glm::vec3(1.0f, 1.0f, 1.0f)) {}
    BoundingBox(glm::vec3 min_corner, glm::vec3 max_corner)
        : min_corner(min_corner), max_corner(max_corner), _original_min_corner(min_corner),
          _original_max_corner(max_corner) {}
};

class BoundingBoxSystem {
    entt::DefaultRegistry &registry;

  public:
    BoundingBoxSystem(entt::DefaultRegistry &registry) : registry(registry) {}
    void update_bounding_boxes() const;
};

#endif // CHAIR_BOUNDINGBOX_HPP