#ifndef LOAD_ASSETS_HPP
#define LOAD_ASSETS_HPP

#include "BoundingBox.hpp"
#include "GpuMaterial.hpp"
#include "GpuMesh.hpp"
#include "RelationalComponents.hpp"
#include "Renderer.hpp"
#include "Transformation.hpp"
#include "asset_tools/Entity.hpp"
#include "asset_tools/Material.hpp"
#include "asset_tools/Mesh.hpp"
#include "chair_config.hpp"
#include "entt/entity/registry.hpp"
#include <fstream>
#include <functional>
#include <iostream>

void load_meshes(std::function<void(asset_tools::Mesh const &&)> callback);
void load_materials(TextureStore &textures, std::function<void(GpuMaterial &&)> callback);

template <typename Entity>
void load_entities(
        entt::Registry<Entity> &registry,
        std::vector<MeshComponent> const &mesh_components,
        std::vector<MaterialComponent> const &material_components,
        std::vector<BoundingBox> const &bounding_boxes) {
    std::ifstream raw_stream(config::ENTITIES_FILE_PATH);
    std::stringstream buffer_stream;
    buffer_stream << raw_stream.rdbuf();

    std::string const buffer = std::move(buffer_stream.str());
    size_t offset = 0;
    std::vector<Parent<Entity>> entities;

    while (offset != buffer.size()) {
        msgpack::unpacked unpacked_data;
        msgpack::unpack(unpacked_data, buffer.data(), buffer.size(), offset);
        std::cerr << unpacked_data.get() << std::endl;
        asset_tools::Entity entity(unpacked_data.get().convert());

        Entity const entity_id = registry.create();
        entities.push_back(Parent(entity_id));

        for (auto const component : entity.components) {
            if (std::holds_alternative<asset_tools::component::Transformation>(component))
                registry.template assign<LocalTransformation>(
                        entity_id, std::get<asset_tools::component::Transformation>(component));

            else if (std::holds_alternative<asset_tools::component::Mesh>(component)) {
                registry.template assign<MeshComponent>(
                        entity_id,
                        mesh_components[std::get<asset_tools::component::Mesh>(component).index]);
                registry.template assign<BoundingBox>(
                        entity_id,
                        bounding_boxes[std::get<asset_tools::component::Mesh>(component).index]);
            }

            else if (std::holds_alternative<asset_tools::component::Material>(component))
                registry.template assign<MaterialComponent>(
                        entity_id,
                        material_components[std::get<asset_tools::component::Material>(component)
                                                    .index]);

            else if (std::holds_alternative<asset_tools::component::Parent>(component))
                registry.template assign<Parent<Entity>>(
                        entity_id,
                        entities[std::get<asset_tools::component::Parent>(component).index]);

            else if (std::holds_alternative<asset_tools::component::BaseEntity>(component))
                registry.template assign<BaseEntity>(
                        entity_id,
                        BaseEntity());

            else
                throw std::runtime_error("Invalid component variant.");
        }
    }
}

#endif