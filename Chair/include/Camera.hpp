#ifndef CHAIR_CAMERA_HPP
#define CHAIR_CAMERA_HPP

// A logical camera class

// Only used as a tag (component) in the ECS.
// - See InputHandler for input related stuff.
// - See Transformation for positional related stuff.
class Camera {};

#endif // CHAIR_CAMERA_HPP
