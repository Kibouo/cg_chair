#ifndef CHAIR_MATERIAL_HPP
#define CHAIR_MATERIAL_HPP

#include "Texture.hpp"
#include "TextureStore.hpp"
#include "asset_tools/Material.hpp"
#include "glad.h"
#include <array>
#include <assimp/material.h>

class GpuMaterial {
    std::shared_ptr<Texture> diffuse_texture;
    std::shared_ptr<Texture> ambient_texture;

    GpuMaterial(std::shared_ptr<Texture> diffuse_texture, std::shared_ptr<Texture> ambient_texture);
    GpuMaterial(GpuMaterial const &) = delete;

  public:
    static GpuMaterial from_asset(asset_tools::Material const &material, TextureStore &textures);
    void bind() const;

    GpuMaterial(GpuMaterial &&);
    ~GpuMaterial();
};

#endif // CHAIR_MATERIAL_HPP
