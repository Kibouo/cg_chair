#include "FpsCounter.hpp"
#include <iostream>

FpsCounter FpsCounter::initialise() {
    auto const last_check = system_clock::now();

    return FpsCounter(last_check);
}

void FpsCounter::handle_frame() {
    this->frames += 1;
    auto const current_frame_time = system_clock::now();
    auto const time_elapsed = current_frame_time - this->last_check;
    if (duration_cast<seconds>(time_elapsed).count() > 1) {
        float const fps = (float)(this->frames * 1000000) /
                (float)duration_cast<microseconds>(time_elapsed).count();
        std::cout << "FPS: " << fps << std::endl;

        this->frames = 0;
        this->last_check = system_clock::now();
    }
}