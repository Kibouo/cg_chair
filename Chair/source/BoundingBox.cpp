#include "BoundingBox.hpp"
#include "Transformation.hpp"
#include <array>
#include <limits>

size_t const AMT_CUBE_CORNERS = 8;

BoundingBox BoundingBox::from_mesh(asset_tools::Mesh const &&mesh) {
    std::vector<asset_tools::Vertex> const vertices = mesh.get_vertices();

    glm::vec3 min_pos = vertices[0].position.to_glm();
    glm::vec3 max_pos = vertices[0].position.to_glm();

    for (size_t i = 1; i < vertices.size(); i++) {
        glm::vec3 const &position = vertices[i].position.to_glm();

        min_pos[0] = std::min(min_pos[0], position[0]);
        min_pos[1] = std::min(min_pos[1], position[1]);
        min_pos[2] = std::min(min_pos[2], position[2]);

        max_pos[0] = std::max(max_pos[0], position[0]);
        max_pos[1] = std::max(max_pos[1], position[1]);
        max_pos[2] = std::max(max_pos[2], position[2]);
    }

    return BoundingBox(
            glm::vec3(min_pos[0], min_pos[1], min_pos[2]),
            glm::vec3(max_pos[0], max_pos[1], max_pos[2]));
}

void BoundingBox::operator=(BoundingBox const &&that) {
    this->min_corner = that.min_corner;
    this->max_corner = that.max_corner;
    this->set_original_min_corner(that.original_min_corner());
    this->set_original_max_corner(that.original_max_corner());
}

// check for intersection with ray and return t.
// This is based on the explanation & code found at
// https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-box-intersection
float const BoundingBox::intersect(
        glm::vec3 const &translation,
        glm::vec3 const &ray_origin,
        glm::vec3 const &inv_look_ray) const {
    float const corner_x_0 = inv_look_ray[0] >= 0.0f ? this->min_corner[0] : this->max_corner[0];
    float const corner_x_1 = inv_look_ray[0] >= 0.0f ? this->max_corner[0] : this->min_corner[0];
    float t_x_min = (corner_x_0 + translation[0] - ray_origin[0]) * inv_look_ray[0];
    float t_x_max = (corner_x_1 + translation[0] - ray_origin[0]) * inv_look_ray[0];

    float const corner_y_0 = inv_look_ray[1] >= 0.0f ? this->min_corner[1] : this->max_corner[1];
    float const corner_y_1 = inv_look_ray[1] >= 0.0f ? this->max_corner[1] : this->min_corner[1];
    float const t_y_min = (corner_y_0 + translation[1] - ray_origin[1]) * inv_look_ray[1];
    float const t_y_max = (corner_y_1 + translation[1] - ray_origin[1]) * inv_look_ray[1];

    if ((t_x_min > t_y_max) || (t_y_min > t_x_max))
        // the ray intersects with the planes but not with the actual box' sides
        return std::numeric_limits<float>::max();
    if (t_y_min > t_x_min)
        t_x_min = t_y_min;
    if (t_y_max < t_x_max)
        t_x_max = t_y_max;

    float const corner_z_0 = inv_look_ray[2] >= 0.0f ? this->min_corner[2] : this->max_corner[2];
    float const corner_z_1 = inv_look_ray[2] >= 0.0f ? this->max_corner[2] : this->min_corner[2];
    float const t_z_min = (corner_z_0 + translation[2] - ray_origin[2]) * inv_look_ray[2];
    float const t_z_max = (corner_z_1 + translation[2] - ray_origin[2]) * inv_look_ray[2];

    if ((t_x_min > t_z_max) || (t_z_min > t_x_max))
        // the ray intersects with the planes but not with the actual box' sides
        return std::numeric_limits<float>::max();
    if (t_z_min > t_x_min)
        t_x_min = t_z_min;
    if (t_z_max < t_x_max)
        t_x_max = t_z_max;

    if (t_x_min < 0 && t_x_max < 0)
        return std::numeric_limits<float>::max();
    if (t_x_min < 0)
        return t_x_max;
    if (t_x_max < 0)
        return t_x_min;
    // both intersections in front of caster? Return closest
    return std::min(t_x_min, t_x_max);
}

// based on http://dev.theomader.com/transform-bounding-boxes/
void BoundingBoxSystem::update_bounding_boxes() const {
    auto const meshes = this->registry.view<Transformation, BoundingBox>();

    for (auto const &mesh : meshes) {
        glm::mat4 const &model_matrix = this->registry.get<Transformation>(mesh).matrix;
        BoundingBox &bound_box = this->registry.get<BoundingBox>(mesh);

        glm::vec3 const x_a = glm::vec3(model_matrix[0] * bound_box.original_min_corner()[0]);
        glm::vec3 const x_b = glm::vec3(model_matrix[0] * bound_box.original_max_corner()[0]);
        glm::vec3 const y_a = glm::vec3(model_matrix[1] * bound_box.original_min_corner()[1]);
        glm::vec3 const y_b = glm::vec3(model_matrix[1] * bound_box.original_max_corner()[1]);
        glm::vec3 const z_a = glm::vec3(model_matrix[2] * bound_box.original_min_corner()[2]);
        glm::vec3 const z_b = glm::vec3(model_matrix[2] * bound_box.original_max_corner()[2]);

        bound_box.min_corner = glm::min(x_a, x_b) + glm::min(y_a, y_b) + glm::min(z_a, z_b);
        bound_box.max_corner = glm::max(x_a, x_b) + glm::max(y_a, y_b) + glm::max(z_a, z_b);
    }
}