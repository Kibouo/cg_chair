#include "ShaderProgram.hpp"
#include "utils.hpp"
#include <iostream>

bool ShaderProgram::linkage_was_success() const {
    GLint success;
    glGetProgramiv(this->shader_program_object, GL_LINK_STATUS, &success);
    return success == 1;
}

void ShaderProgram::print_linkage_log() const {
    GLint log_length;
    glGetProgramiv(this->shader_program_object, GL_INFO_LOG_LENGTH, &log_length);

    if (log_length > 0) {
        std::unique_ptr<GLchar> log(new GLchar[log_length]);
        glGetShaderInfoLog(this->shader_program_object, log_length, nullptr, log.get());
        std::cerr << log.get() << std::endl;
    }
}

void ShaderProgram::link(std::vector<Shader const *> const &shaders) const {
    for (auto const &shader : shaders)
        glAttachShader(this->shader_program_object, shader->get_raw_object_handle());
    glLinkProgram(this->shader_program_object);

    this->print_linkage_log();

    if (!this->linkage_was_success())
        throw std::runtime_error("failed to link shader program");
}

ShaderProgram::ShaderProgram(
        VertexShader const &vertex_shader, FragmentShader const &fragment_shader)
    : shader_program_object(glCreateProgram()) {
    this->link({&vertex_shader, &fragment_shader});
}

ShaderProgram::ShaderProgram(
        VertexShader const &vertex_shader,
        GeometryShader const &geometry_shader,
        FragmentShader const &fragment_shader)
    : shader_program_object(glCreateProgram()) {
    this->link({&vertex_shader, &geometry_shader, &fragment_shader});
}

ShaderProgram::ShaderProgram(ComputeShader const &compute_shader)
    : shader_program_object(glCreateProgram()) {
    this->link({&compute_shader});
}

ShaderProgram::~ShaderProgram() { glDeleteProgram(this->shader_program_object); }

void ShaderProgram::use() const { glUseProgram(this->shader_program_object); }

void ShaderProgram::operator=(ShaderProgram &&that) {
    glDeleteProgram(this->shader_program_object);
    this->shader_program_object = that.shader_program_object;
    that.shader_program_object = 0;
}

ShaderProgram::ShaderProgram(ShaderProgram &&that)
    : shader_program_object(that.shader_program_object) {
    that.shader_program_object = 0;
}