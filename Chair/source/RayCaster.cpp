#include "RayCaster.hpp"
#include "BoundingBox.hpp"
#include "RelationalComponents.hpp"
#include "Renderer.hpp"
#include <array>
#include <iostream>
#include <limits>
#include <vector>

template <typename Entity>
RayCaster<Entity> RayCaster<Entity>::initialise(
        std::vector<GpuMesh> const &meshes, entt::Registry<Entity> &registry) {
    std::filesystem::path const compute_shader_path("shaders/compute.cs.glsl");
    ShaderProgram compute_shader = ShaderProgram(ComputeShader(compute_shader_path));

    return RayCaster<Entity>(meshes, registry, std::move(compute_shader));
}

template <typename Entity>
glm::vec3 const RayCaster<Entity>::calc_look_ray(
        Transformation const &camera_transformation,
        int const window_width_pixels,
        int const window_height_pixels,
        float const aspect_ratio) const {
    /// viewport coords
    // We're lazy and hardcode the cursor position as windowsize/2. The cursor is centered
    // anyway.
    //
    // Watch out! All calculations are with origin left-bottom, however glfw returns left-top.
    //  Thus, if you decide to calculate cursor position then convert y to the correct origin.
    int const cursor_x = window_width_pixels / 2;
    int const cursor_y = window_height_pixels / 2;

    /// normalized device coords
    // each dimension is now in [-1:1]
    float const ndc_x = ((2.0f * cursor_x) / window_width_pixels) - 1.0f;
    float const ndc_y = ((2.0f * cursor_y) / window_height_pixels) - 1.0f;

    /// homogeneous clip coords
    // z = -1.0f is the "forward" direction in OpenGl
    glm::vec4 const clip_ray = glm::vec4(ndc_x, ndc_y, -1.0f, 1.0f);

    /// eye coords
    glm::vec4 eye_ray = glm::inverse(Renderer::calc_perspective_matrix(aspect_ratio)) * clip_ray;
    // we want a vector pointing forward
    eye_ray = glm::vec4(eye_ray[0], eye_ray[1], -1.0f, 0.0f);

    /// world coords
    // to go back from eye to world do inverse(view_matrix) * ray.
    // However, view_matrix is inverse(camera_transformation) and thus to save calculations
    //  we use camera_transformation directly.
    return glm::normalize(glm::vec3(camera_transformation.matrix * eye_ray));
}

template <typename Entity>
Entity const RayCaster<Entity>::base_entity_of(Entity const &entity) const {
    Entity tmp_entity = entity;
    while (!this->registry.template has<BaseEntity>(tmp_entity))
        tmp_entity = this->registry.template get<Parent<Entity>>(tmp_entity).identifier;

    return tmp_entity;
}

template <typename Entity>
void RayCaster<Entity>::initiate_triangle_level_cast(
        glm::vec3 const &origin,
        glm::vec3 const &look_ray,
        std::tuple<Entity, bool, float> const &entity_tuple) {
    MeshComponent const &mesh_comp =
            this->registry.template get<MeshComponent>(std::get<0>(entity_tuple));
    glm::mat4 const &transformation =
            this->registry.template get<Transformation>(std::get<0>(entity_tuple)).matrix;

    this->compute_shader.use();
    this->meshes.at(mesh_comp.id).bind_to_compute();
    GLuint const amt_triangles = this->meshes.at(mesh_comp.id).index_count / 3;

    // buffer 2
    GLuint distances_buffer;
    glGenBuffers(1, &distances_buffer);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, distances_buffer);
    glBufferData(GL_SHADER_STORAGE_BUFFER, amt_triangles * sizeof(float), nullptr, GL_STREAM_READ);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, distances_buffer);

    // uniforms
    glUniform1f(0, std::numeric_limits<float>::max());
    glUniform1ui(1, amt_triangles);
    glUniform3fv(2, 1, &origin[0]);
    glUniform3fv(3, 1, &look_ray[0]);
    glUniformMatrix4fv(4, 1, GL_FALSE, &transformation[0][0]);

    // dispatch for each triangle a work group
    GLint work_group_count;
    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &work_group_count);
    // even though the gpu can theoretically handle work_group_count amount of work groups, it will
    // most likely lock up if given that many groups. Thus we take half of that amount as maximum.
    GLuint const work_group_count_unsigned =
            std::min(amt_triangles, (GLuint)(work_group_count / 2));
    glDispatchCompute(work_group_count_unsigned, 1, 1);

    this->triangle_cast_data.push_back({entity_tuple, distances_buffer, amt_triangles});
}

template <typename Entity>
std::tuple<Entity, bool, float> RayCaster<Entity>::evaluate_triangle_level_casts() {
    // wait for gpu to finish
    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

    // read results
    std::tuple<Entity, bool, float> closest_entity = {0, false, std::numeric_limits<float>::max()};
    for (auto data : this->triangle_cast_data) {
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, std::get<1>(data));
        float const *const distances = (float *)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);
        float closest_distance = std::numeric_limits<float>::max();
        for (size_t i = 0; i < std::get<2>(data); i++)
            closest_distance = std::min(closest_distance, distances[i]);

        if (closest_distance < std::get<2>(closest_entity))
            closest_entity = {std::get<0>(std::get<0>(data)), true, closest_distance};

        glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
        glDeleteBuffers(1, &std::get<1>(data));
    }

    this->triangle_cast_data.clear();

    return closest_entity;
}

template <typename Entity>
std::tuple<Entity, bool, float> const RayCaster<Entity>::cast(
        Transformation const &camera_transformation, glm::vec3 const &look_ray) {
    glm::vec3 const inv_look_ray = 1.0f / look_ray;

    std::vector<std::tuple<Entity, bool, float>> hit_entities;
    auto const models = this->registry.template view<Transformation, BoundingBox>();
    for (auto const model : models) {
        BoundingBox const &bound_box = this->registry.template get<BoundingBox>(model);
        glm::vec3 const translation =
                this->registry.template get<Transformation>(model).translation();

        float const distance_factor =
                bound_box.intersect(translation, camera_transformation.translation(), inv_look_ray);

        if (distance_factor < std::numeric_limits<float>::max())
            hit_entities.push_back({model, true, distance_factor});
    }

    for (auto &hit_entity : hit_entities)
        // Go test on triangle level if AABB was hit
        if (std::get<1>(hit_entity))
            this->initiate_triangle_level_cast(
                    camera_transformation.translation(), look_ray, hit_entity);

    std::tuple<Entity, bool, float> closest_entity = evaluate_triangle_level_casts();

    // Get base entity of closest hit entity
    if (std::get<1>(closest_entity))
        std::get<0>(closest_entity) = this->base_entity_of(std::get<0>(closest_entity));

    return closest_entity;
}

template struct RayCaster<std::uint32_t>;