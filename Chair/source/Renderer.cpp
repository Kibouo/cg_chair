#include "Renderer.hpp"
#include "Window.hpp"
#include "glad.h"
#include "utils.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <sstream>
#include <stdexcept>

/**
 * Initialises a VBO and a VAO to render a quad.
 * @return An array containing a VBO and a VAO in order.
 */
std::array<GLuint, 2> initialise_quad();

Renderer::Renderer(
        std::shared_ptr<Window> window,
        ShaderProgram geometry_pass,
        ShaderProgram flat_geometry_pass,
        ShaderProgram deferred_pass,
        Framebuffer framebuffer,
        GLuint quad_vbo,
        GLuint quad_vao)
    : window(std::move(window)), geometry_pass(std::move(geometry_pass)),
      flat_geometry_pass(std::move(flat_geometry_pass)), deferred_pass(std::move(deferred_pass)),
      framebuffer(std::move(framebuffer)), quad_vbo(quad_vbo), quad_vao(quad_vao), meshes(),
      materials(), flat_mode_active(false), wireframe_mode_active(false) {
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glfwSwapInterval(0);

    print_gl_errors();
    this->deferred_pass.use();
}

Renderer Renderer::initialise(std::shared_ptr<Window> window) {
    auto const [screen_width, screen_height] = window->size();
    auto const [quad_vbo, quad_vao] = initialise_quad();

    VertexShader const vertex_shader(std::filesystem::path("shaders/deferred/stage1.vs.glsl"));
    GeometryShader const geometry_shader(std::filesystem::path("shaders/flat/stage1.gs.glsl"));
    FragmentShader const fragment_shader(std::filesystem::path("shaders/deferred/stage1.fs.glsl"));

    return Renderer(
            std::move(window), ShaderProgram(vertex_shader, fragment_shader),
            ShaderProgram(vertex_shader, geometry_shader, fragment_shader),
            ShaderProgram(
                    VertexShader(std::filesystem::path("shaders/deferred/stage2.vs.glsl")),
                    FragmentShader(std::filesystem::path("shaders/deferred/stage2.fs.glsl"))),
            Framebuffer::initialise(screen_width, screen_height), quad_vbo, quad_vao);
}

template <typename Entity>
bool Renderer::render_frame(entt::Registry<Entity> &registry, LightManager<Entity> &light_manager) {
    Transformation const &camera_transformation =
            registry.template get<Transformation>(registry.template attachee<Camera>());

    { // geometry pass
        glm::mat4 const view_matrix = calc_view_matrix(camera_transformation);
        glm::mat4 const perspective_matrix = calc_perspective_matrix(this->window->aspect_ratio());

        if (this->is_wireframe_mode_active())
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

        if (this->is_flat_mode_active())
            this->flat_geometry_pass.use();
        else
            this->geometry_pass.use();

        this->framebuffer.setup_geometry_pass();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);

        auto entities = registry.template view<MeshComponent, MaterialComponent, Transformation>();
        for (auto entity : entities) {
            glm::mat4 const model_matrix = entities.template get<Transformation>(entity).matrix;
            glUniformMatrix4fv(0, 1, GL_FALSE, &view_matrix[0][0]);
            glUniformMatrix4fv(1, 1, GL_FALSE, &perspective_matrix[0][0]);
            glUniformMatrix4fv(2, 1, GL_FALSE, &model_matrix[0][0]);
            this->materials[entities.template get<MaterialComponent>(entity).id].bind();
            this->meshes[entities.template get<MeshComponent>(entity).id].draw();
        }
    }

    { // deferred pass
        if (this->is_wireframe_mode_active())
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        this->deferred_pass.use();
        this->framebuffer.setup_deferred_pass();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // bind lights ubo
        light_manager.bind();
        // Pass eye_position
        glUniform3fv(1, 1, &camera_transformation.translation()[0]);

        glBindVertexArray(this->quad_vao);
        glDisable(GL_DEPTH_TEST);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        glBindVertexArray(0);
    }
    return this->window->swap_buffers();
}
template bool Renderer::render_frame<std::uint32_t>(
        entt::Registry<std::uint32_t> &registry, LightManager<std::uint32_t> &light_manager);

Renderer::~Renderer() {
    glDeleteVertexArrays(1, &this->quad_vao);
    glDeleteBuffers(1, &this->quad_vbo);
}

Renderer::Renderer(Renderer &&that)
    : window(std::move(that.window)), geometry_pass(std::move(that.geometry_pass)),
      flat_geometry_pass(std::move(that.flat_geometry_pass)),
      deferred_pass(std::move(that.deferred_pass)), framebuffer(std::move(that.framebuffer)),
      quad_vbo(that.quad_vbo), quad_vao(that.quad_vao), meshes(std::move(that.meshes)),
      materials(std::move(that.materials)), flat_mode_active(that.flat_mode_active),
      wireframe_mode_active(that.wireframe_mode_active) {
    that.quad_vbo = 0;
    that.quad_vao = 0;
}

size_t Renderer::mesh_count() const { return this->meshes.size(); }

size_t Renderer::material_count() const { return this->materials.size(); }

bool Renderer::is_wireframe_mode_active() const { return this->wireframe_mode_active; }
bool Renderer::is_flat_mode_active() const { return this->flat_mode_active; }
void Renderer::set_wireframe_mode_active(bool active) { this->wireframe_mode_active = active; }
void Renderer::set_flat_mode_active(bool active) { this->flat_mode_active = active; }

glm::mat4 const Renderer::calc_view_matrix(Transformation const &camera_transformation) {
    return glm::inverse(camera_transformation.matrix);
}

glm::mat4 const Renderer::calc_perspective_matrix(float const aspect_ratio) {
    return glm::perspective(glm::radians(FOV_DEGREE), aspect_ratio, Z_NEAR, Z_FAR);
}

MaterialComponent Renderer::add_material(GpuMaterial &&material) {
    std::vector<GpuMaterial>::size_type const id = this->materials.size();
    this->materials.push_back(std::move(material));
    return MaterialComponent(id);
}

MeshComponent Renderer::add_mesh(GpuMesh &&mesh) {
    std::vector<GpuMesh>::size_type const id = this->meshes.size();
    this->meshes.push_back(std::move(mesh));
    return MeshComponent(id);
}

std::array<GLuint, 2> initialise_quad() {
    std::array<glm::vec2, 6> const vertices{glm::vec2(-1, -1), glm::vec2(1, -1), glm::vec2(1, 1),
                                            glm::vec2(-1, -1), glm::vec2(1, 1),  glm::vec2(-1, 1)};

    GLuint vbo;
    GLuint vao;

    glGenBuffers(1, &vbo);
    glGenVertexArrays(1, &vao);

    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices.data(), GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return {vbo, vao};
}

MeshComponent::MeshComponent(std::vector<GpuMesh>::size_type id) : id(id) {}
MaterialComponent::MaterialComponent(std::vector<GpuMaterial>::size_type id) : id(id) {}