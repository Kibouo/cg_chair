#include "Texture.hpp"
#include <stdexcept>
#include <utility>
#include <iostream>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

Texture::Texture(GLuint texture_object) : texture_object(texture_object) {}

std::shared_ptr<Texture> Texture::from_file(char const *filename) {
    int width;
    int height;
    int channel_count;

    stbi_set_flip_vertically_on_load(true);
    float *const data = stbi_loadf(filename, &width, &height, &channel_count, 3);

    if (data == nullptr) {
        std::stringstream error;
        error << "Failed to load image '" << filename << "'.";
        throw std::runtime_error(error.str());
    }
    
    auto const texture = Texture::from_data(data, width, height);

    stbi_image_free(data);

    return texture;
}

std::shared_ptr<Texture> Texture::from_colour(asset_tools::Vector<float, 3> const colour) {
    return Texture::from_data(&colour, 1, 1);
}

std::shared_ptr<Texture>
Texture::from_data(GLvoid const *const data, GLuint const width, GLuint const height) {
    GLuint gl_texture;

    glGenTextures(1, &gl_texture);
    glBindTexture(GL_TEXTURE_2D, gl_texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT, data);glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glBindTexture(GL_TEXTURE_2D, 0);

    return std::shared_ptr<Texture>(new Texture(gl_texture));
}

Texture::Texture(Texture &&that) : texture_object(that.texture_object) { that.texture_object = 0; }
Texture::~Texture() { glDeleteTextures(1, &this->texture_object); }
void Texture::operator=(Texture &&that) {
    glDeleteTextures(1, &this->texture_object);
    this->texture_object = that.texture_object;
    that.texture_object = 0;
}

void Texture::bind(GLenum index) const {
    glActiveTexture(GL_TEXTURE0 + index);
    glBindTexture(GL_TEXTURE_2D, this->texture_object);
}