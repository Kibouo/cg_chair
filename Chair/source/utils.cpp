#include "utils.hpp"
#include "glad.h"
#include <fstream>
#include <iostream>
#include <string>

void print_gl_errors() {
    while (true) {
        std::string error_msg;
        GLenum error = glGetError();
        switch (error) {
            case GL_NO_ERROR:
                return;
            case GL_INVALID_ENUM:
                error_msg = "GL_INVALID_ENUM";
                break;
            case GL_INVALID_VALUE:
                error_msg = "GL_INVALID_VALUE";
                break;
            case GL_INVALID_OPERATION:
                error_msg = "GL_INVALID_OPERATION";
                break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                error_msg = "GL_INVALID_FRAMEBUFFER_OPERATION";
                break;
            case GL_OUT_OF_MEMORY:
                error_msg = "GL_OUT_OF_MEMORY";
                break;
            default:
                error_msg = "UNKNOWN GL ERROR";
                break;
        }
        std::cout << error_msg << std::endl;
    }
}

std::vector<char> read_file(std::filesystem::path const &path) {
    std::ifstream input_file(path, std::ios::in | std::ifstream::binary);

    std::istreambuf_iterator<char> start_iterator((input_file));
    std::istreambuf_iterator<char> end_iterator;

    return std::vector<char>(start_iterator, end_iterator);
}

std::string read_file_to_string(std::filesystem::path const &path) {
    std::stringstream stream;
    stream << std::ifstream(path).rdbuf();
    return stream.str();
}