#include "FramerateCompensator.hpp"

FramerateCompensator FramerateCompensator::initialise() {
    auto const last_frame_time = system_clock::now();

    return FramerateCompensator(last_frame_time);
}

void FramerateCompensator::count_created_time() {
    auto const current_frame_time = system_clock::now();
    this->accumulated_time +=
            (double)duration_cast<microseconds>(current_frame_time - last_frame_time).count();
    this->last_frame_time = current_frame_time;
}

void FramerateCompensator::consume_time(std::function<void()> const &time_sensitive_fn) {
    while (this->accumulated_time >= FRAME_DURATION_MICROSEC) {
        time_sensitive_fn();

        this->accumulated_time -= FRAME_DURATION_MICROSEC;
    }
}
