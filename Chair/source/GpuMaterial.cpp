#include "GpuMaterial.hpp"
#include "chair_config.hpp"
#include <filesystem>
#include <iostream>
#include <memory>

/**
 * Helper function to convert the relative texture paths into paths starting in the textures
 * directory.
 */
std::string into_texture_path(std::string const &path) {
    std::filesystem::path texture_path(config::TEXTURES_PATH);
    texture_path.append(path);
    return texture_path.string();
}

GpuMaterial GpuMaterial::from_asset(asset_tools::Material const &material, TextureStore &textures) {
    std::optional<std::shared_ptr<Texture>> ambient_texture;
    std::optional<std::shared_ptr<Texture>> diffuse_texture;

    
    { // Load ambient texture.
        if (material.ambient_texture.has_value())
            ambient_texture =
                    textures.load_texture_from_file(into_texture_path(*material.ambient_texture));
        else if (material.ambient_colour.has_value())
            ambient_texture = textures.load_texture_from_colour(*material.ambient_colour);
        else {
            std::cerr << "Material did not have am ambient texture. Defaulting to black."
                      << std::endl;
            ambient_texture =
                    textures.load_texture_from_colour(asset_tools::Vector<float, 3>(0.0, 00, 0.0));
        }
    }

    { // Load diffuse texture.
        if (material.diffuse_texture.has_value())
            diffuse_texture =
                    textures.load_texture_from_file(into_texture_path(*material.diffuse_texture));
        else if (material.diffuse_colour.has_value())
            diffuse_texture = textures.load_texture_from_colour(*material.diffuse_colour);
        else {
            std::cerr << "Material did not have am diffuse texture. Defaulting to black."
                      << std::endl;
            diffuse_texture =
                    textures.load_texture_from_colour(asset_tools::Vector<float, 3>(0.0, 00, 0.0));
        }
    }
    return GpuMaterial(*diffuse_texture, *ambient_texture);
}

GpuMaterial::GpuMaterial(
        std::shared_ptr<Texture> diffuse_texture, std::shared_ptr<Texture> ambient_texture)
    : diffuse_texture(std::move(diffuse_texture)), ambient_texture(std::move(ambient_texture)){};

void GpuMaterial::bind() const {
    this->diffuse_texture->bind(0);
    this->ambient_texture->bind(1);
}

GpuMaterial::GpuMaterial(GpuMaterial &&that)
    : diffuse_texture(std::move(that.diffuse_texture)),
      ambient_texture(std::move(that.ambient_texture)) {}

GpuMaterial::~GpuMaterial() {}
