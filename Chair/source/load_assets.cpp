#include "load_assets.hpp"

void load_meshes(std::function<void(asset_tools::Mesh const &&)> callback) {
    std::ifstream raw_stream(config::MESHES_FILE_PATH);
    std::stringstream buffer_stream;
    buffer_stream << raw_stream.rdbuf();

    std::string const buffer = std::move(buffer_stream.str());
    size_t offset = 0;

    while (offset != buffer.size()) {
        msgpack::unpacked unpacked_data;
        msgpack::unpack(unpacked_data, buffer.data(), buffer.size(), offset);

        msgpack::object const object = unpacked_data.get();
        callback(object.convert());
    }
}

void load_materials(TextureStore &textures, std::function<void(GpuMaterial &&)> callback) {
    std::ifstream raw_stream(config::MATERIALS_FILE_PATH);
    std::stringstream buffer_stream;
    buffer_stream << raw_stream.rdbuf();

    std::string const buffer = std::move(buffer_stream.str());
    size_t offset = 0;

    while (offset != buffer.size()) {
        msgpack::unpacked unpacked_data;
        msgpack::unpack(unpacked_data, buffer.data(), buffer.size(), offset);
        msgpack::object const object = unpacked_data.get();

        asset_tools::Material const material(object.convert());
        callback(GpuMaterial::from_asset(material, textures));
    }
}
