#include "glad.h"

#include "BoundingBox.hpp"
#include "FpsCounter.hpp"
#include "FramerateCompensator.hpp"
#include "GpuMaterial.hpp"
#include "InputHandler.hpp"
#include "LightManager.hpp"
#include "RayCaster.hpp"
#include "RelationalComponents.hpp"
#include "Renderer.hpp"
#include "TextureStore.hpp"
#include "Transformation.hpp"
#include "Window.hpp"
#include "load_assets.hpp"
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <iostream>
#include <string>

void handle_glfw_error(int code, char const *message) {
    std::cerr << "GLFW ERROR " << code << std::endl;
    std::cerr << message << std::endl;
}

std::tuple<glm::vec3, glm::vec3, glm::vec3> get_node_transformation(aiNode const &node) {
    aiVector3D scaling;
    aiVector3D rotation;
    aiVector3D position;

    node.mTransformation.Decompose(scaling, rotation, position);

    return {glm::vec3(position.x, position.y, position.z),
            glm::vec3(rotation.x, rotation.y, rotation.z),
            glm::vec3(scaling.x, scaling.y, scaling.z)};
}

template <typename Entity>
void init_player(entt::Registry<Entity> &registry) {
    Entity const player = registry.create();

    registry.template assign<LocalTransformation>(
            player, glm::vec3(-150.0f, 40.0f, 425.0f), glm::vec3(90.0f, 180.0f, 0.0f),
            glm::vec3(1.0f, 1.0f, 1.0f));
    registry.template attach<Camera>(player);
}

int main() {
    glfwSetErrorCallback(handle_glfw_error);

    std::shared_ptr<Window> window = Window::initialise();
    Window *window_ptr = window.get();

    glDebugMessageCallback(
            [](GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei size,
               GLchar const *message, void const *_) { std::cerr << message << std::endl; },
            nullptr);

    Renderer renderer = Renderer::initialise(std::move(window));

    entt::DefaultRegistry registry;
    TextureStore texture_store;

    std::vector<MeshComponent> mesh_components;
    std::vector<BoundingBox> bounding_boxes;
    std::vector<MaterialComponent> material_components;

    load_meshes([&](asset_tools::Mesh const &&mesh) {
        mesh_components.push_back(renderer.add_mesh(GpuMesh(mesh)));
        bounding_boxes.push_back(BoundingBox::from_mesh(std::move(mesh)));
    });
    std::cerr << "Loaded " << renderer.mesh_count() << " meshes." << std::endl;
    load_materials(texture_store, [&](GpuMaterial &&material) {
        material_components.push_back(renderer.add_material(std::move(material)));
    });
    std::cerr << "Loaded " << renderer.material_count() << " materials." << std::endl;
    load_entities(registry, mesh_components, material_components, bounding_boxes);
    std::cerr << "Loaded entities." << std::endl;

    // The ray caster needs meshes to test colission on triangle level.
    // Ray casting is done on input (i.e. mouse click)
    RayCaster<std::uint32_t> ray_caster =
            RayCaster<std::uint32_t>::initialise(renderer.meshes, registry);
    std::cerr << "Created ray caster." << std::endl;
    InputHandler<std::uint32_t> input_handler =
            InputHandler<std::uint32_t>::initialise(window_ptr, ray_caster);
    std::cerr << "Created input handler." << std::endl;

    init_player(registry);
    std::cerr << "Created player." << std::endl;
    LightManager<std::uint32_t> light_manager = LightManager<std::uint32_t>::initialise(registry);
    light_manager.init_lights();
    std::cerr << "Loaded lights." << std::endl;

    TransformationSystem transformation_system(registry);
    std::cerr << "Created transformation system." << std::endl;
    BoundingBoxSystem bounding_box_system(registry);
    std::cerr << "Created bounding box system." << std::endl;

    FpsCounter fps_counter = FpsCounter::initialise();
    FramerateCompensator frame_comp = FramerateCompensator::initialise();

    std::cerr << "Starting main loop." << std::endl;

    transformation_system.update_transformations();
    while (renderer.render_frame(registry, light_manager)) {
        fps_counter.handle_frame();

        frame_comp.count_created_time();
        frame_comp.consume_time([&]() {
            // main loop actions
            input_handler.enact_all(registry, renderer);
            transformation_system.update_transformations();
            bounding_box_system.update_bounding_boxes();
        });
    }

    return 0;
}
