#include "Transformation.hpp"
#include "Camera.hpp"
#include "RelationalComponents.hpp"
#include <glm/gtc/matrix_transform.hpp>

void TransformationSystem::update_locals() {
    auto const locals = this->registry.view<LocalTransformation>();

    // Update all entities that have a LocalTransformation with a Transformation.
    for (auto const entity : locals)
        this->registry.accommodate<Transformation>(entity, locals.get(entity).to_matrix());
}

LocalTransformation::LocalTransformation(
        asset_tools::component::Transformation const &transformation)
    : translation(transformation.translation.to_glm()), rotation(transformation.rotation.to_glm()),
      scaling(transformation.scaling.to_glm()) {}

void TransformationSystem::update_transformations() {
    // TODO: use this-> for entities for clearer code.
    this->update_locals();

    auto const parents = this->registry.view<Parent<std::uint32_t>>();

    bool entities_updated = false;
    for (auto entity : parents) {
        auto const new_index = this->entities.size();
        auto const [iter, inserted] =
                this->entity_indices.insert(std::pair<std::uint32_t, size_t>(entity, new_index));
        if (inserted) {
            this->entities.push_back(entity);
            entities_updated = true;
        }
    }

    if (entities_updated)
        this->update_order();

    for (auto const entity : this->entities) {
        auto const parent = this->registry.get<Parent<std::uint32_t>>(entity).identifier;
        glm::mat4 &local_matrix = this->registry.get<Transformation>(entity).matrix;
        glm::mat4 const &parent_matrix = this->registry.get<Transformation>(parent).matrix;

        local_matrix = parent_matrix * local_matrix;
    }
}

void TransformationSystem::update_order() {
    std::vector<std::uint32_t>::size_type index = 0;
    while (index != this->entities.size()) {
        auto const entity = this->entities[index];
        auto const parent = this->registry.get<Parent<std::uint32_t>>(entity).identifier;

        auto const entity_iter = this->entity_indices.find(entity);
        auto const parent_iter = this->entity_indices.find(parent);

        if (parent_iter == this->entity_indices.cend() ||
            parent_iter->second <= entity_iter->second)
            index += 1;
        else {
            auto &entity_index = entity_iter->second;
            auto &parent_index = parent_iter->second;

            std::swap(this->entities[entity_index], this->entities[parent_index]);
            std::swap(entity_index, parent_index);
        }
    }
}

TransformationSystem::TransformationSystem(entt::DefaultRegistry &registry) : registry(registry) {}

LocalTransformation::LocalTransformation(
        glm::vec3 const translation, glm::vec3 const rotation, glm::vec3 const scaling)
    : translation(translation), rotation(rotation), scaling(scaling) {}

LocalTransformation::LocalTransformation()
    : translation(0, 0, 0), rotation(0, 0, 0), scaling(1, 1, 1) {}

glm::mat4 LocalTransformation::to_matrix() const {
    glm::mat4 matrix(1.0f);
    {
        matrix = glm::translate(matrix, this->translation);
        matrix = glm::rotate(matrix, this->rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));
        matrix = glm::rotate(matrix, this->rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
        matrix = glm::rotate(matrix, this->rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
        matrix = glm::scale(matrix, this->scaling);
    }
    return matrix;
}

Transformation::Transformation() : matrix() {}

Transformation::Transformation(glm::mat4 matrix) : matrix(matrix) {}

glm::vec3 Transformation::translation() const { return glm::vec3(matrix[3]); }
