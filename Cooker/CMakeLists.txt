﻿# CMakeList.txt : CMake project for Cooker, include source and define
# project specific logic here.
#
cmake_minimum_required (VERSION 3.8)

project(Cooker)

include_directories("${ASSIMP_INCLUDE_DIRS}" "${INCLUDE_DIRECTORIES_GLM}" "${INCLUDE_DIRECTORIES_MSGPACK}" "${INCLUDE_DIRECTORIES_BOOST_FS}")

# Add source to this project's executable.
add_executable (Cooker "main.cpp")
target_link_libraries(Cooker "${ASSIMP_LIBRARIES}" AssetTools)

if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
  target_link_libraries(Cooker stdc++fs)
endif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")

set(ASSETS_DIRECTORY "${CMAKE_BINARY_DIR}/assets")

add_custom_target(MakeAssetsDirectory ALL
    COMMAND ${CMAKE_COMMAND} -E make_directory ${ASSETS_DIRECTORY})

add_custom_target(Cook
  COMMAND Cooker "${CMAKE_SOURCE_DIR}/objects/scene.pile_000.fbx" "${CMAKE_SOURCE_DIR}/objects/scene.pile_001.fbx" "${CMAKE_SOURCE_DIR}/objects/scene.pile_002.fbx" "${CMAKE_SOURCE_DIR}/objects/scene.pile_003.fbx" "${CMAKE_SOURCE_DIR}/objects/scene.pile_004.fbx" "${CMAKE_SOURCE_DIR}/objects/scene.pile_005.fbx" "${CMAKE_SOURCE_DIR}/objects/scene.pile_006.fbx" "${CMAKE_SOURCE_DIR}/objects/scene.lamp.fbx" "${CMAKE_SOURCE_DIR}/objects/scene.floor.fbx"
  WORKING_DIRECTORY "${ASSETS_DIRECTORY}"
  BYPRODUCTS "${ASSETS_DIRECTORY}/entities.meal" "${ASSETS_DIRECTORY}/materials.meal" "${ASSETS_DIRECTORY}/meshes.meal"
  COMMENT "Cooking assets."
  DEPENDS MakeAssetsDirectory
)
