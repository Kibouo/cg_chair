#pragma once

#ifndef IG_MESH_HPP
#define IG_MESH_HPP

#include "asset_tools/Vector.hpp"
#include "assimp/mesh.h"
#include "glm/glm.hpp"
#include "msgpack.hpp"
#include <stdexcept>
#include <string>
#include <vector>

namespace asset_tools {

    struct Vertex {
        Vector<float, 3> position;
        Vector<float, 3> normal;
        Vector<float, 2> texture_coordinates;

        Vertex(Vector<float, 3> const &position,
               Vector<float, 3> const &normal,
               Vector<float, 2> const texture_coordinates) throw();

        Vertex()
            : position(0.0f, 0.0f, 0.0f), normal(0.0f, 0.0f, 0.0f),
              texture_coordinates(0.0f, 0.0f){};

        MSGPACK_DEFINE(position, normal, texture_coordinates);
    };

    class Mesh {
        std::vector<Vertex> vertices;
        std::vector<std::uint32_t> indices;

      public:
        Mesh();
        Mesh(aiMesh const &mesh);
        MSGPACK_DEFINE(vertices, indices);

        std::vector<Vertex> const &get_vertices() const;
        std::vector<std::uint32_t> const &get_indices() const;
    };

    struct MeshParseException : public std::runtime_error {
        MeshParseException(std::string const &what) throw();
    };

    struct NoNormalsException : public MeshParseException {
        NoNormalsException() throw();
    };

    struct NonTriangularFaceException : public MeshParseException {
        NonTriangularFaceException(size_t const vertices_count) throw();
    };
} // namespace asset_tools
#endif