#ifndef IG_COMPONENT_HPP
#define IG_COMPONENT_HPP

#include "asset_tools/Vector.hpp"
#include "msgpack.hpp"
#include <cstdint>
#include <iostream>
#include <stdexcept>
#include <variant>

namespace asset_tools {

    enum class ComponentType { Transformation, Mesh, Material, Parent };

    namespace component {

        struct Transformation {
            Vector<float, 3> translation;
            Vector<float, 3> rotation;
            Vector<float, 3> scaling;

            Transformation() throw();
            Transformation(
                    Vector<float, 3> const &translation,
                    Vector<float, 3> const &rotation,
                    Vector<float, 3> const &scaling) throw();

            MSGPACK_DEFINE(translation, rotation, scaling);
        };

        struct Mesh {
            std::uint32_t index;

            Mesh() throw();
            Mesh(std::uint32_t index) throw();

            MSGPACK_DEFINE(index);
        };

        struct Material {
            std::uint32_t index;

            Material() throw();
            Material(std::uint32_t index) throw();

            MSGPACK_DEFINE(index);
        };

        struct Parent {
            std::uint32_t index;

            Parent() throw();
            Parent(std::uint32_t index) throw();

            MSGPACK_DEFINE(index);
        };

        struct BaseEntity {
            std::uint32_t index;

            BaseEntity() throw();
            BaseEntity(std::uint32_t index) throw();

            MSGPACK_DEFINE(index);
        };
    } // namespace component

    typedef std::variant<
            component::Transformation,
            component::Mesh,
            component::Material,
            component::Parent,
            component::BaseEntity>
            ComponentVariant;

} // namespace asset_tools

namespace msgpack {
    MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS) {
        namespace adaptor {
            template <>
            struct as<asset_tools::ComponentVariant> {
                asset_tools::ComponentVariant operator()(msgpack::object const &object) const {
                    if (object.type != msgpack::type::ARRAY || object.via.array.size != 2) {
                        std::cerr << "shots fired" << std::endl;
                        throw msgpack::type_error();
                    }

                    std::uint32_t const type_index = object.via.array.ptr[0].as<std::uint32_t>();

                    switch (type_index) {
                        case 0:
                            return object.via.array.ptr[1]
                                    .as<asset_tools::component::Transformation>();
                        case 1:
                            return object.via.array.ptr[1].as<asset_tools::component::Mesh>();
                        case 2:
                            return object.via.array.ptr[1].as<asset_tools::component::Material>();
                        case 3:
                            return object.via.array.ptr[1].as<asset_tools::component::Parent>();
                        case 4:
                            return object.via.array.ptr[1].as<asset_tools::component::BaseEntity>();
                        default:
                            throw std::runtime_error(
                                    "Invalid component variant while deserialising.");
                    }
                }
            };

            template <>
            struct convert<asset_tools::ComponentVariant> {
                msgpack::object const &operator()(
                        msgpack::object const &object,
                        asset_tools::ComponentVariant &component) const {
                    component = object.as<asset_tools::ComponentVariant>();
                    return object;
                }
            };

            template <>
            struct pack<asset_tools::ComponentVariant> {
                template <typename Stream>
                msgpack::packer<Stream> &operator()(
                        msgpack::packer<Stream> &stream,
                        asset_tools::ComponentVariant const &component_variant) const {
                    stream.pack_array(2);
                    stream.pack<std::uint32_t>(component_variant.index());

                    if (std::holds_alternative<asset_tools::component::Transformation>(
                                component_variant))
                        stream.pack(std::get<asset_tools::component::Transformation>(
                                component_variant));

                    else if (std::holds_alternative<asset_tools::component::Mesh>(
                                     component_variant))
                        stream.pack(std::get<asset_tools::component::Mesh>(component_variant));

                    else if (std::holds_alternative<asset_tools::component::Material>(
                                     component_variant))
                        stream.pack(std::get<asset_tools::component::Material>(component_variant));

                    else if (std::holds_alternative<asset_tools::component::Parent>(
                                     component_variant))
                        stream.pack(std::get<asset_tools::component::Parent>(component_variant));

                    else if (std::holds_alternative<asset_tools::component::BaseEntity>(
                                     component_variant))
                        stream.pack(
                                std::get<asset_tools::component::BaseEntity>(component_variant));

                    else
                        throw std::runtime_error("Invalid component variant.");

                    return stream;
                }
            };
        } // namespace adaptor
    }     // namespace adaptor
} // namespace msgpack

#endif