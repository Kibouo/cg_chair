#pragma once

#ifndef IG_MATERIAL_HPP
#define IG_MATERIAL_HPP

#include "asset_tools/Vector.hpp"
#include "assimp/material.h"
#include <optional>
#include <string>

namespace asset_tools {

    struct Material {
        std::optional<Vector<float, 3>> ambient_colour;
        std::optional<Vector<float, 3>> diffuse_colour;

        std::optional<std::string> ambient_texture;
        std::optional<std::string> diffuse_texture;

        Material();
        Material(aiMaterial const &ai_material);

        MSGPACK_DEFINE(ambient_colour, diffuse_colour, ambient_texture, diffuse_texture);
    };
} // namespace asset_tools
#endif