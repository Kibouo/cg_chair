#include "asset_tools/Material.hpp"

namespace asset_tools {

    Vector<float, 3> ai_colour_to_vector(aiColor4D const &ai_colour) throw() {
        return Vector<float, 3>(ai_colour.r, ai_colour.g, ai_colour.b);
    }

    std::optional<Vector<float, 3>> extract_diffuse_colour(aiMaterial const &ai_material) {
        aiColor4D ai_diffuse_colour;
        if (AI_SUCCESS == ai_material.Get(AI_MATKEY_COLOR_AMBIENT, ai_diffuse_colour))
            return std::optional(ai_colour_to_vector(ai_diffuse_colour));
        else
            return std::optional<Vector<float, 3>>();
    }

    std::optional<Vector<float, 3>> extract_ambient_colour(aiMaterial const &ai_material) {
        aiColor4D ai_ambient_colour;
        if (AI_SUCCESS == ai_material.Get(AI_MATKEY_COLOR_AMBIENT, ai_ambient_colour))
            return std::optional(ai_colour_to_vector(ai_ambient_colour));
        else
            return std::optional<Vector<float, 3>>();
    }

    std::optional<std::string> extract_diffuse_texture(aiMaterial const &ai_material) {
        aiString texture_filename;
        if (AI_SUCCESS == ai_material.Get(AI_MATKEY_TEXTURE_DIFFUSE(0), texture_filename))
            return std::optional(std::string(texture_filename.C_Str()));
        else
            return std::optional<std::string>();
    }

    std::optional<std::string> extract_ambient_texture(aiMaterial const &ai_material) {
        aiString texture_filename;
        if (AI_SUCCESS == ai_material.Get(AI_MATKEY_TEXTURE_AMBIENT(0), texture_filename))
            return std::optional(std::string(texture_filename.C_Str()));
        else
            return std::optional<std::string>();
    }

    Material::Material() {}

    Material::Material(aiMaterial const &ai_material)
        : diffuse_colour(extract_diffuse_colour(ai_material)),
          ambient_colour(extract_ambient_colour(ai_material)),
          diffuse_texture(extract_diffuse_texture(ai_material)),
          ambient_texture(extract_ambient_texture(ai_material)) {}
} // namespace asset_tools