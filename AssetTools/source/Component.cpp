#include "asset_tools/Component.hpp"

MSGPACK_ADD_ENUM(asset_tools::ComponentType);

namespace asset_tools {
    namespace component {
        Transformation::Transformation() throw()
            : Transformation(
                      Vector<float, 3>(0.0f, 0.0f, 0.0f),
                      Vector<float, 3>(0.0f, 0.0f, 0.0f),
                      Vector<float, 3>(1.0f, 1.0f, 1.0f)) {}

        Transformation::Transformation(
                Vector<float, 3> const &translation,
                Vector<float, 3> const &rotation,
                Vector<float, 3> const &scaling) throw()
            : translation(translation), rotation(rotation), scaling(scaling) {}

        Mesh::Mesh() throw() {}
        Material::Material() throw() {}
        Parent::Parent() throw() {}
        BaseEntity::BaseEntity() throw() {}

        Mesh::Mesh(uint32_t const index) throw() : index(index) {}
        Material::Material(uint32_t const index) throw() : index(index) {}
        Parent::Parent(uint32_t const index) throw() : index(index) {}
    } // namespace component

} // namespace asset_tools