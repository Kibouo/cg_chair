#include "asset_tools/Entity.hpp"
#include <tuple>
#include <utility>

namespace asset_tools {
    Entity::Entity(std::vector<ComponentVariant> components) throw()
        : components(std::move(components)) {}

    /// Returns the translation, rotation and scaling of the node, in that order.
    std::tuple<Vector<float, 3>, Vector<float, 3>, Vector<float, 3>>
    extract_transformation(aiNode const &ai_node) {
        aiVector3D ai_translation;
        aiVector3D ai_rotation;
        aiVector3D ai_scaling;

        ai_node.mTransformation.Decompose(ai_scaling, ai_rotation, ai_translation);

        return {Vector<float, 3>(ai_translation), Vector<float, 3>(ai_rotation),
                Vector<float, 3>(ai_scaling)};
    }

    Entity::Entity() {}

    void Entity::extract_hierarchy(
            aiScene const &ai_scene,
            aiNode const &ai_node,
            std::function<size_t(Entity &&entity)> const &callback,
            size_t mesh_offset,
            size_t material_offset,
            std::optional<size_t> parent) throw() {

        auto const [translation, rotation, scaling] = extract_transformation(ai_node);
        component::Transformation transformation(translation, rotation, scaling);

        // An entity can only have one mesh. In assimp a node can have multiple meshes. If there is
        // more than one mesh, create a parent entity to contain each of them as a seperate child
        // entity. As to not create an excessive amount of entities, we only do this when needed.
        //
        // This has the side-effect that meshes of a parent-node in assimp might become siblings of
        // the child-nodes.
        if (ai_node.mNumMeshes > 1) {
            std::vector<ComponentVariant> components{
                    component::Transformation(translation, rotation, scaling)};

            if (parent.has_value())
                components.emplace_back(component::Parent(*parent));
            else
                components.emplace_back(component::BaseEntity());

            // Get the index of the entity we just registered.
            parent = callback(Entity(std::move(components)));
            transformation = component::Transformation();
        }

        // Create nodes for every mesh.
        for (size_t mesh_index = 0; mesh_index < ai_node.mNumMeshes; mesh_index += 1) {
            size_t const material_index = ai_scene.mMeshes[mesh_index]->mMaterialIndex;

            std::vector<ComponentVariant> components{
                    transformation, component::Mesh(mesh_index + mesh_offset),
                    component::Material(material_index + material_offset)};

            if (parent.has_value())
                components.emplace_back(component::Parent(*parent));
            else
                components.emplace_back(component::BaseEntity());

            callback(Entity(std::move(components)));
        }

        // Parse all the child entities.
        for (size_t node_index = 0; node_index < ai_node.mNumChildren; node_index += 1)
            Entity::extract_hierarchy(
                    ai_scene, *ai_node.mChildren[node_index], callback, mesh_offset,
                    material_offset, parent);
    }
} // namespace asset_tools